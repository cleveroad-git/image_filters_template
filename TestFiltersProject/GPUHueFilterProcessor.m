//
//  GPUHueFilterProcessor.m
//  TestFiltersProject
//
//  Created by Gromov on 26.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import "GPUHueFilterProcessor.h"

@implementation GPUHueFilterProcessor

- (instancetype)initWithInputImage:(UIImage *)inputImage imageView:(GPUImageView *)imageView
{
    self = [super initWithInputImage:inputImage imageView:imageView];
    if (self) {
        _filter = [[GPUImageHueFilter alloc] init];
        _sourcePicture = [[GPUImagePicture alloc] initWithImage:inputImage smoothlyScaleOutput:YES];
        [_sourcePicture addTarget:_filter];
        [_filter addTarget:imageView];
        [(GPUImageHueFilter *)_filter setHue:0.0];
        [_sourcePicture processImage];
    }
    return self;
}

- (void)setSlider:(UISlider *)slider
{
    [slider setMinimumValue:0.0];
    [slider setMaximumValue:360.0];
    [slider setValue:0.0];
}

- (void)changeFilterValue:(CGFloat)newValue
{
    [(GPUImageHueFilter *)_filter setHue:newValue];
    [_sourcePicture processImage];
}

@end
