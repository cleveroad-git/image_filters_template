//
//  ViewController.m
//  TestFiltersProject
//
//  Created by Gromov on 24.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import "ViewController.h"
#import "GPUImage.h"
#import "GPUFilterProcessor.h"

@interface ViewController () {
    IBOutlet UIView * imageView;
    IBOutlet UIButton * contrastButton;
    IBOutlet UIButton * brightnessButton;
    IBOutlet UIButton * saturationButton;
    IBOutlet UIButton * hueButton;
    IBOutlet UIButton * sharpnessButton;
    IBOutlet UIButton * doneButton;
    IBOutlet UIButton * cancelButton;
    IBOutlet UISlider * slider;
    
    GPUImageView * gpuImageView;
    GPUFilterProcessor * processor;
    UIImage * tempImage;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    tempImage = [UIImage imageNamed:@"WID-small.jpg"];
    [self showImage:tempImage];
}

- (void)showImage:(UIImage *)image
{
    GPUImagePicture * inputImage = [[GPUImagePicture alloc] initWithImage:image smoothlyScaleOutput:YES];
    [inputImage processImage];
    gpuImageView = [self getGPUImageView];
    [imageView addSubview:gpuImageView];
    [inputImage addTarget: gpuImageView];
}

- (GPUImageView *)getGPUImageView
{
    if (gpuImageView) {
        [gpuImageView removeFromSuperview];
    }
    gpuImageView = [[GPUImageView alloc] initWithFrame:imageView.frame];
    [imageView addSubview:gpuImageView];
    return gpuImageView;
}

- (void)hideButtons
{
    contrastButton.hidden = YES;
    brightnessButton.hidden = YES;
    saturationButton.hidden = YES;
    sharpnessButton.hidden = YES;
    hueButton.hidden = YES;
    doneButton.hidden = NO;
    cancelButton.hidden = NO;
    slider.hidden = NO;
}

- (void)showButtons
{
    contrastButton.hidden = NO;
    brightnessButton.hidden = NO;
    saturationButton.hidden = NO;
    sharpnessButton.hidden = NO;
    hueButton.hidden = NO;
    doneButton.hidden = YES;
    cancelButton.hidden = YES;
    slider.hidden = YES;
}

- (IBAction)contrastButtonClick
{
    [self makeFilterByType:GPUIMAGE_CONTRAST];
}

- (IBAction)brightnessButtonClick
{
    [self makeFilterByType:GPUIMAGE_BRIGHTNESS];
}

- (IBAction)saturationButtonClick
{
    [self makeFilterByType:GPUIMAGE_SATURATION];
}

- (IBAction)hueButtonClick
{
    [self makeFilterByType:GPUIMAGE_HUE];
}

- (IBAction)sharpnessButtonClick
{
    [self makeFilterByType:GPUIMAGE_SHARPNESS];
}

- (void)makeFilterByType:(GPUImageShowcaseFilterType)type
{
    [self hideButtons];
    gpuImageView = [self getGPUImageView];
    processor = [GPUFilterProcessor getProcessorByType:type inputImage:tempImage imageView:gpuImageView];
    [processor setSlider:slider];
}

- (IBAction)doneButtonClick
{
    [self showButtons];
    tempImage = [processor image];
    [self showImage:tempImage];
}

- (IBAction)cancelButtonClick
{
    [self showButtons];
    processor = nil;
    [self showImage:tempImage];
}


- (IBAction)updateFilterFromSlider:(UISlider *)sender
{
    [processor changeFilterValue:sender.value];
}

@end
