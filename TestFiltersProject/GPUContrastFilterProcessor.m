//
//  GPUContrastFilterProcessor.m
//  TestFiltersProject
//
//  Created by Gromov on 24.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import "GPUContrastFilterProcessor.h"

@implementation GPUContrastFilterProcessor

- (instancetype)initWithInputImage:(UIImage *)inputImage imageView:(GPUImageView *)imageView
{
    self = [super initWithInputImage:inputImage imageView:imageView];
    if (self) {
        _filter = [[GPUImageContrastFilter alloc] init];
        _sourcePicture = [[GPUImagePicture alloc] initWithImage:inputImage smoothlyScaleOutput:YES];
        [_sourcePicture addTarget:_filter];
        [_filter addTarget:imageView];
        [_sourcePicture processImage];
    }
    return self;
}

- (void)setSlider:(UISlider *)slider
{
    [slider setMinimumValue:0.3];
    [slider setMaximumValue:3.0];
    [slider setValue:1.0];
}

- (void)changeFilterValue:(CGFloat)newValue
{
    [(GPUImageContrastFilter *)_filter setContrast:newValue];
    [_sourcePicture processImage];
}

@end
