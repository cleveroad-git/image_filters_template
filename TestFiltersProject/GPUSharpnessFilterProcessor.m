//
//  GPUSharpnessFilterProcessor.m
//  TestFiltersProject
//
//  Created by Gromov on 26.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import "GPUSharpnessFilterProcessor.h"

@implementation GPUSharpnessFilterProcessor

- (instancetype)initWithInputImage:(UIImage *)inputImage imageView:(GPUImageView *)imageView
{
    self = [super initWithInputImage:inputImage imageView:imageView];
    if (self) {
        _filter = [[GPUImageSharpenFilter alloc] init];
        _sourcePicture = [[GPUImagePicture alloc] initWithImage:inputImage smoothlyScaleOutput:YES];
        [_sourcePicture addTarget:_filter];
        [_filter addTarget:imageView];
        [(GPUImageSharpenFilter *)_filter setSharpness:0.0];
        [_sourcePicture processImage];
    }
    return self;
}

- (void)setSlider:(UISlider *)slider
{
    [slider setMinimumValue:-1.0];
    [slider setMaximumValue:4.0];
    [slider setValue:0.0];
}

- (void)changeFilterValue:(CGFloat)newValue
{
    NSLog(@"%f", newValue);
    [(GPUImageSharpenFilter *)_filter setSharpness:newValue];
    [_sourcePicture processImage];
}

@end
