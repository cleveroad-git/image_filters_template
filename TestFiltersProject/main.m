//
//  main.m
//  TestFiltersProject
//
//  Created by Gromov on 24.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
