//
//  GPUFilterProcessor.h
//  TestFiltersProject
//
//  Created by Gromov on 24.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GPUImage.h"

typedef enum {
    GPUIMAGE_SATURATION,
    GPUIMAGE_CONTRAST,
    GPUIMAGE_BRIGHTNESS,
    GPUIMAGE_HUE,
    GPUIMAGE_SHARPNESS,
} GPUImageShowcaseFilterType;

// Abstract class

@interface GPUFilterProcessor : NSObject {
@protected
    GPUImageOutput<GPUImageInput> * _filter;
    GPUImagePicture * _sourcePicture;
    UIImage * _inputImage;
    GPUImageView * _imageView;
}

+ (GPUFilterProcessor *)getProcessorByType:(GPUImageShowcaseFilterType)type
                                inputImage:(UIImage *)inputImage
                                 imageView:(GPUImageView *)imageView;

- (instancetype)initWithInputImage:(UIImage *)inputImage imageView:(GPUImageView *)imageView;

- (void)setSlider:(UISlider *)slider;        ///pure virtual function
- (void)changeFilterValue:(CGFloat)newValue; ///pure virtual function
- (UIImage *)image;

@end
