//
//  GPUBrightnessProcessor.m
//  TestFiltersProject
//
//  Created by Gromov on 25.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import "GPUBrightnessProcessor.h"

@implementation GPUBrightnessProcessor

- (instancetype)initWithInputImage:(UIImage *)inputImage imageView:(GPUImageView *)imageView
{
    self = [super initWithInputImage:inputImage imageView:imageView];
    if (self) {
        _filter = [[GPUImageBrightnessFilter alloc] init];
        _sourcePicture = [[GPUImagePicture alloc] initWithImage:inputImage smoothlyScaleOutput:YES];
        [_sourcePicture addTarget:_filter];
        [_filter addTarget:imageView];
        [_sourcePicture processImage];
    }
    return self;
}

- (void)setSlider:(UISlider *)slider
{
    [slider setMinimumValue:-0.5];
    [slider setMaximumValue:0.5];
    [slider setValue:0.0];
}

- (void)changeFilterValue:(CGFloat)newValue
{
    [(GPUImageBrightnessFilter *)_filter setBrightness:newValue];
    [_sourcePicture processImage];
}

@end
