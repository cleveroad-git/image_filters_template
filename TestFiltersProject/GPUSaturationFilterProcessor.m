//
//  GPUSaturationFilterProcessor.m
//  TestFiltersProject
//
//  Created by Gromov on 25.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import "GPUSaturationFilterProcessor.h"

@implementation GPUSaturationFilterProcessor

- (instancetype)initWithInputImage:(UIImage *)inputImage imageView:(GPUImageView *)imageView
{
    self = [super initWithInputImage:inputImage imageView:imageView];
    if (self) {
        _filter = [[GPUImageSaturationFilter alloc] init];
        _sourcePicture = [[GPUImagePicture alloc] initWithImage:inputImage smoothlyScaleOutput:YES];
        [_sourcePicture addTarget:_filter];
        [_filter addTarget:imageView];
        [_sourcePicture processImage];
    }
    return self;
}

- (void)setSlider:(UISlider *)slider
{
    [slider setMinimumValue:0.0];
    [slider setMaximumValue:2.0];
    [slider setValue:1.0];
}

- (void)changeFilterValue:(CGFloat)newValue
{
    [(GPUImageSaturationFilter *)_filter setSaturation:newValue];
    [_sourcePicture processImage];
}

@end
