//
//  GPUFilterProcessor.m
//  TestFiltersProject
//
//  Created by Gromov on 24.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import "GPUFilterProcessor.h"
#import "GPUContrastFilterProcessor.h"
#import "GPUBrightnessProcessor.h"
#import "GPUSaturationFilterProcessor.h"
#import "GPUHueFilterProcessor.h"
#import "GPUSharpnessFilterProcessor.h"

@interface GPUFilterProcessor ()

@end

@implementation GPUFilterProcessor

+ (GPUFilterProcessor *)getProcessorByType:(GPUImageShowcaseFilterType)type
                                inputImage:(UIImage *)inputImage
                                 imageView:(GPUImageView *)imageView
{
    switch (type) {
        case GPUIMAGE_CONTRAST:
            return [[GPUContrastFilterProcessor alloc] initWithInputImage:inputImage imageView:imageView];
            break;
            
        case GPUIMAGE_BRIGHTNESS:
            return [[GPUBrightnessProcessor alloc] initWithInputImage:inputImage imageView:imageView];
            break;
            
        case GPUIMAGE_SATURATION:
            return [[GPUSaturationFilterProcessor alloc] initWithInputImage:inputImage imageView:imageView];
            break;
            
        case GPUIMAGE_HUE:
            return [[GPUHueFilterProcessor alloc] initWithInputImage:inputImage imageView:imageView];
            break;
            
        case GPUIMAGE_SHARPNESS:
            return [[GPUSharpnessFilterProcessor alloc] initWithInputImage:inputImage imageView:imageView];
            break;
            
        default:
            break;
    }
    return nil;
}

- (instancetype)initWithInputImage:(UIImage *)inputImage imageView:(GPUImageView *)imageView
{
    self = [super init];
    if (self) {
//        _filter = filter;
        _inputImage = inputImage;
        _imageView = imageView;
    }
    return self;
}

- (void)setSlider:(UISlider *)slider
{
    [[NSException exceptionWithName:@"Abstract class exeption" reason:@"Pure virtual function call" userInfo:nil] raise];
}

- (void)changeFilterValue:(CGFloat)newValue
{
    [[NSException exceptionWithName:@"Abstract class exeption" reason:@"Pure virtual function call" userInfo:nil] raise];
}

- (UIImage *)image
{
    [_filter useNextFrameForImageCapture];
    [_sourcePicture processImage];
    return [_filter imageFromCurrentFramebuffer];
}

@end
